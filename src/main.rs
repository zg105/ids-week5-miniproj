use std::collections::HashMap;
use mysql_async::{prelude::Queryable, Row, Pool};

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    // connect to AWS MySQL
    let pool = Pool::new("mysql://admin:gzcgzc0721@week5.cvys4s6o2i31.us-east-1.rds.amazonaws.com:3306/week5");
    let mut conn = pool.get_conn().await?;

    // Perform the query
    let rows: Vec<Row> = conn.query("SELECT sum(quantity) AS sum FROM week5").await?;

    // Extract the 'sum' column as i64 from the first row
    let result: Option<i64> = rows.get(0).and_then(|row| row.get("sum"));

    // Check if we got a result and print it
    match result {
        Some(sum) => println!("Sum of quantity: {}", sum),
        None => println!("No data found or sum is NULL"),
    }

    Ok(())
}
