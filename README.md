# ids-week5-miniProj


## Tasks

- Create a Rust AWS Lambda function
- Implement a simple service
- Connect to a databse (MySQL)

## Steps

1. Install Rust, Cargo Lambda and AWS CLI
2. Create a new Rust project using `cargo lambda new <project_name>`
3. Initialize a RDS MySQL database
![](pic/2.png)
4. implement Lambda Function in the src/main.rs file
5. Test the Lambda Function by running `cargo run`
![](pic/3.png)
6. Build the Lambda Function by running `cargo lambda build --release`
7. Run `cargo lambda deploy` to deploy the Lambda Function to AWS
8. Run `aws lambda list-functions` to see the functions
![](pic/4.png)
9. add an API Gateway for Lambda Function
![](pic/1.png)
10. Invoke the Lambda Function both by endpoint or CLI
![](pic/5.png)
